# Params file for variables

#### GLANCE
variable "image" {
  type    = string
  default = "centos-7"
}

#### NEUTRON
variable "external_network" {
  type    = string
  default = "internet_vlan109"
}

variable "dns_ip" {
  type    = list(string)
  default = ["8.8.8.8", "8.8.8.4"]
}

#### VM parameters
variable "flavor_http" {
  type    = string
  default = "small"
}

variable "network_http" {
  type = map(string)
  default = {
    subnet_name = "103.27.201.0/24"
    cidr        = "103.27.201.0/24"
  }
}
