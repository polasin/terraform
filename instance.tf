#### INSTANCE HTTP ####
#
# Create instance
#
resource "openstack_compute_keypair_v2" "user_key" {
  name  = "polasin"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDsBOhG42pQqnqoSJfB16qWloO7yzKomPT4MdV34fxYRsiThAAw6pvjsu0bWtLx1Gi4bXNSJmq5Dr+b6nrR7QW7TTF3GzFzrHpjl/vHBtxUK9cdL1P/iNemh20QFtDxCrUQpSxRoa7KEiD2D97phluNi+Vaxcn1yuzPo8DiNGT0BipZLJ6hb7JkiJLeazEGFcB27o7e/afkbiLh9QaHWpijaIuygZNYyjjlm4tZnwWj6mYwVegU64YIAvOdPnE8IZPwY6kwBbqUtW/N/0JfXHQcbCqXx3yJWrZbgQ7PMnj6h+7el1t8sEO4KiUlAQMydRm6G4O5+AMKdtLc2LFAK2fJztHaeEGzzdtZggsNEtLLb6vMM7B2HBhf7pCZV4ekApxNXmgHdJj4vExDJxhUbFXKO8kAxvqBxdpdNRaWp4LRFPRzflttQR7/j4zYwwL+JVbRqxOM2BIw3MVEbiXvdm3zvYcpJkg8Wukg/HhPTsCgB+YB2VNE8VqGmnfaj7knWFU= Nine@DESKTOP-B77IODD"
}
resource "openstack_images_image_v2" "centos7" {
  name             = "Jelastic-cs-user-node_CentOS-7"
  disk_format      = "qcow2"
  container_format = "bare"
  visibility       = "shared"
  min_disk_gb      = 1208
}
resource "openstack_compute_instance_v2" "jelastic-th06-cs" {
  name        = "th06.cs.ruk-com.cloud"
  image_name  = openstack_images_image_v2.centos7.name
  flavor_name = "jelastic-user-node"
  key_pair    = openstack_compute_keypair_v2.user_key.name
  user_data   = file("user-data.sh")
  block_device {
    uuid                  = openstack_images_image_v2.centos7.id    
    source_type           = "image"
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 1208
    delete_on_termination = true
  }
  network {
    name = "internet108"
    fixed_ip_v4 = "103.86.48.245"
  }
  network {
    name = "jelastic_private_cslox"
    fixed_ip_v4 = "10.103.0.6"
  }
  
}

