# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name   = "xxxxxxxxx"
  tenant_name = "xxxxxxxxx"
  domain_name = "proen"
  password    = "xxxxxxxxx"
  auth_url    = "https://proen.rhi.ruk-com.cloud:5000/v3"
  region      = "RegionOne"
}

